﻿using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient.Reguests
{
    class UserRequest
    {
        public string ApiUrl = "https://localhost:44309";
        public async Task<List<User>> GetUsersAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<User> userss = new List<User>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Users");
                if (response.IsSuccessStatusCode)
                {
                    userss = JsonConvert.DeserializeObject<List<User>>(await response.Content.ReadAsStringAsync());
                }
                return userss;
            }
        }

        public async Task<User> GetUserAsync(int userId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                User user = new User();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Users/{0}", userId));
                if (response.IsSuccessStatusCode)
                {
                    user = JsonConvert.DeserializeObject<User>(await response.Content.ReadAsStringAsync());
                }
                return user;
            }
        }
    }
}
