﻿using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ConsoleClient.Reguests
{
    public class ComplexValueRequest : IDisposable
    {
        public string ApiUrl = "https://localhost:44309";

        HttpClient httpClient;
        public ComplexValueRequest()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(ApiUrl);
        }

        public async System.Threading.Tasks.Task<Dictionary<int, int>> GetCountTasksInProjectByAuthorId(List<Project> projects, int userId)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetCountTasksInProjectByAuthorId?id={0}", userId));
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Dictionary<int, int>>(await response.Content.ReadAsStringAsync());
            }
            return null;
        }


        public async System.Threading.Tasks.Task<List<Task>> GetUserTasksWithShortNameByUserId(List<Project> projects, int userId)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetUserTasksWithShortNameByUserId?id={0}", userId));
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<List<Task>>(await response.Content.ReadAsStringAsync());
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<Task>> GetUserTasksFinishedInCurrentYear(List<Project> projects, int userId)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetUserTasksFinishedInCurrentYear?id={0}", userId));
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<List<Task>>(await response.Content.ReadAsStringAsync());
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<(int Id, string Name, List<User> Teammates)>> GetListOfTeamsWithTeamatesOlderThen10(List<Project> projects)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetListOfTeamsWithTeamatesOlderThen10"));
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<List<(int Id, string Name, List<User> Teammates)>>(await response.Content.ReadAsStringAsync());
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<(User User, List<Task> Tasks)>> GetListOfUsersAscendingWhitTasksDescending(List<Project> projects)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetListOfUsersAscendingWhitTasksDescending"));
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<List<(User User, List<Task> Tasks)>>(await response.Content.ReadAsStringAsync());
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<(User User, Project LastProject, List<Task> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, Task LongesUserTask)>>
            GetUserWithLastProjectAndETC(List<Project> projects)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetUserWithLastProjectAndETC"));
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<List<(User User, Project LastProject, List<Task> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, Task LongesUserTask)>>(await response.Content.ReadAsStringAsync());
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<(Project Project, Task TaskWithLongestDescriptions, Task TaskWithShortestName, int CountOfUsersInProjects)>>
            GetProjectWithTaskWithLongestDescriptionsAndETC(List<Project> projects)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetProjectWithTaskWithLongestDescriptionsAndETC"));
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<List<(Project Project, Task TaskWithLongestDescriptions, Task TaskWithShortestName, int CountOfUsersInProjects)>>(await response.Content.ReadAsStringAsync());
            }
            return null;
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }

    }
}
