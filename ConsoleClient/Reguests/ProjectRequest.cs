﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Common.Models;
using Newtonsoft.Json;

namespace ConsoleClient.Reguests
{
    public class ProjectRequest
    {
        public string ApiUrl = "https://localhost:44309";
        public async Task<List<Project>> GetProjectsAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<Project> projects = new List<Project>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Projects");
                if (response.IsSuccessStatusCode)
                {
                    projects = JsonConvert.DeserializeObject<List<Project>>(await response.Content.ReadAsStringAsync());
                }
                return projects;
            }
        }
        public async Task<Project> GetProjectAsync(int projectId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Project project = new Project();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Projects/{0}", projectId));
                if (response.IsSuccessStatusCode)
                {
                    project = JsonConvert.DeserializeObject<Project>(await response.Content.ReadAsStringAsync());
                }
                return project;
            }
        }
    }
}
