﻿using System;
using System.Collections.Generic;
using Common.Models;
using ConsoleClient.Reguests;
using System.Linq;

namespace ConsoleClient
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var complexValueRequest = new ComplexValueRequest();
            var projects = GetProjectsAsync();
            while (true)
            {
                Console.WriteLine("1. Get the number of tasks from the project of a specific\nuser.\n");
                Console.WriteLine("2. Get a list of tasks assigned to a specific user,\nwhere name is a task < 45 characters(a collection of tasks).\n");
                Console.WriteLine("3. Get the tasks that\n are finished(finished) in the current(2021) year for a \nspecific user(by id).\n");
                Console.WriteLine("4 .Get a list(id, team name and list of users) from\nteams whose members are over 10 years old, sorted by user\nregistration date in descending order,\n and also grouped by teams\n.");
                Console.WriteLine("5. Get a list of users alphabetically sorted(ascending)\nwith tasks sorted by name length(descending).\n");
                Console.WriteLine("6. Get the following structure(pass user Id to parameters):");
                Console.WriteLine("User");
                Console.WriteLine("User's last project (by creation date)");
                Console.WriteLine("Total number of tasks under the last project");
                Console.WriteLine("Total number of unfinished or canceled tasks for the user");
                Console.WriteLine("Longest user task by date(first created - last completed).\n");
                Console.WriteLine("7.Get the following structure:");
                Console.WriteLine("Project");
                Console.WriteLine("The longest task of the project(by description)");
                Console.WriteLine("Shortest project task(by name)");
                Console.WriteLine("The total number of users in the project team, where either\nthe project description > 20 characters or the number of tasks < 3");
                Console.WriteLine("\n\nEnter the menu item number: ");
                int selectedItem = 0;
                if (int.TryParse(Console.ReadLine(), out selectedItem))
                {
                    int selectedUser = 0;
                    if (selectedItem < 4)
                    {
                        Console.WriteLine("Enter user Id:");
                        if (int.TryParse(Console.ReadLine(), out selectedUser))
                        {
                            if (!((new UserRequest().GetUserAsync(selectedUser)) != null))
                            {
                                Console.WriteLine("User not found.");
                                Console.ReadKey();
                                Console.Clear();
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("The entered value is not a valid numeric value.");
                            Console.ReadKey();
                            Console.Clear();
                            continue;
                        }
                    }
                    switch (selectedItem)
                    {
                        case 1:
                            var CountTasksInProjectByAuthorId = await complexValueRequest.GetCountTasksInProjectByAuthorId(projects, selectedUser);
                            if (CountTasksInProjectByAuthorId == null || CountTasksInProjectByAuthorId?.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var pair in CountTasksInProjectByAuthorId)
                            {
                                Console.WriteLine("ProjectId: " + pair.Key + " Number of tasks:" + pair.Value);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 2:
                            var UserTasksWithShortNameByUserId = await complexValueRequest.GetUserTasksWithShortNameByUserId(projects, selectedUser);
                            if (UserTasksWithShortNameByUserId == null || UserTasksWithShortNameByUserId?.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in UserTasksWithShortNameByUserId)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 3:
                            var UserTasksFinishedInCurrentYear = await complexValueRequest.GetUserTasksFinishedInCurrentYear(projects, selectedUser);
                            if (UserTasksFinishedInCurrentYear == null || UserTasksFinishedInCurrentYear.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in UserTasksFinishedInCurrentYear)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 4:
                            var ListOfTeamsWithTeamatesOlderThen10 = await complexValueRequest.GetListOfTeamsWithTeamatesOlderThen10(projects);
                            if (ListOfTeamsWithTeamatesOlderThen10 == null ||  ListOfTeamsWithTeamatesOlderThen10?.Count() == 0)
                            {
                                Console.WriteLine("No teams");
                                break;
                            }
                            foreach (var team in ListOfTeamsWithTeamatesOlderThen10)
                            {
                                string userInfo = "";
                                foreach (var teammate in team.Teammates)
                                {
                                    userInfo += teammate.ToString() + "\n";
                                }
                                Console.WriteLine(string.Format("\nTeam:\n[\nId:{0},\nName:{1},\nUsers:{2}\n]\n", team.Id, team.Name, userInfo));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                            }
                            break;
                        case 5:
                            var ListOfUsersAscendingWhitTasksDescending = await complexValueRequest.GetListOfUsersAscendingWhitTasksDescending(projects);
                            if (ListOfUsersAscendingWhitTasksDescending == null || ListOfUsersAscendingWhitTasksDescending?.Count() == 0)
                            {
                                Console.WriteLine("No results");
                                break;
                            }
                            foreach (var tuple in ListOfUsersAscendingWhitTasksDescending)
                            {
                                string taskInfo = "";
                                foreach (var task in tuple.Tasks)
                                {
                                    taskInfo += task.ToString() + "\n";
                                }
                                Console.WriteLine(string.Format("\nTuple:\n[\nUser:{0},\nTasks:{1}\n]\n", tuple.User, taskInfo));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                            }
                            break;
                        case 6:
                            var UserWithLastProjectAndETC = await complexValueRequest.GetUserWithLastProjectAndETC(projects);
                            if (UserWithLastProjectAndETC == null || UserWithLastProjectAndETC?.Count() == 0)
                            {
                                Console.WriteLine("No results");
                                break;
                            }
                            foreach (var tuple in UserWithLastProjectAndETC)
                            {
                                string taskInfo = "";
                                if (tuple.LastProjectTask != null)
                                {
                                    foreach (var task in tuple.LastProjectTask)
                                    {
                                        taskInfo += task.ToString() + "\n";
                                    }
                                }
                                Console.WriteLine(string.Format("\nTuple:\n[\nUser:{0},\nLast projects:{1},\nLast project tasks:{2},\nCount of finished or canceled tasks:{3},\nLongest task:{4}\n]\n", tuple.User, tuple.LastProject, taskInfo, tuple.CountOfUnfinishedAndCanceledTasks, tuple.LongesUserTask));
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        case 7:
                            var ProjectWithTaskWithLongestDescriptionsAndETC = await complexValueRequest.GetProjectWithTaskWithLongestDescriptionsAndETC(projects);
                            if (ProjectWithTaskWithLongestDescriptionsAndETC == null || ProjectWithTaskWithLongestDescriptionsAndETC?.Count() == 0)
                            {
                                Console.WriteLine("No tasks");
                                break;
                            }
                            foreach (var task in ProjectWithTaskWithLongestDescriptionsAndETC)
                            {
                                Console.WriteLine(task);
                                Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                            }
                            break;
                        default:
                            Console.WriteLine("There are no item with this number.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("The entered value is not a valid numeric value.");
                }
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
                Console.Clear();
            }
        }
        public static List<Project> GetProjectsAsync()
        {
            return new ProjectRequest().GetProjectsAsync().Result;
        }
    }
}
