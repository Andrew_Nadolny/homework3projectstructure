﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.DTO
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
