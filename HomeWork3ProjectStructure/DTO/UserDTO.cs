﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork3ProjectStructure.DTO
{
    public class UserDTO : Entity
    {
        //public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
