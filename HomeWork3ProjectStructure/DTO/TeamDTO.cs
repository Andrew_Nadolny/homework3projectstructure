﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork3ProjectStructure.DTO
{
    public class TeamDTO : Entity
    {
        //public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
