﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork3ProjectStructure.DTO
{
    public class ProjectDTO : Entity
    {
        //public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
