﻿using HomeWork3ProjectStructure.Repositores;
using HomeWork3ProjectStructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.UnitOfWork
{
    public class UnitOfWork
    {
        public IRepository<TEntity> Set<TEntity>() where TEntity : Entity
        {
            return new Repository<TEntity>();
        }

        public int SaveChanges()
        {
            return 1;
        }

        public async Task<int> SaveChangesAsync()
        {
            return 1;
        }
    }
}
