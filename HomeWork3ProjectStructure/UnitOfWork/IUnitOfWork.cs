﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.Repositores;
using HomeWork3ProjectStructure.DTO;


namespace HomeWork3ProjectStructure.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> Set<TEntity>() where TEntity : Entity;

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }
}
