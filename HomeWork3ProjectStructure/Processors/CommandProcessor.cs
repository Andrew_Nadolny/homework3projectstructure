﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Interfaces;
using HomeWork3ProjectStructure.Commands;
using Common.Models;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Processors
{
    public class CommandProcessor : ICommandProcessor
    {
        private CommandHandler _commandHandler { get; set; }
        public CommandProcessor(CommandHandler commandHandler)
        {
            _commandHandler = commandHandler;
        }

        public async Task<Project> ProcessedAsync(CreateProjectCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<Common.Models.Task> ProcessedAsync(CreateTaskCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<Team> ProcessedAsync(CreateTeamCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<User> ProcessedAsync(CreateUserCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<Project> ProcessedAsync(UpdateProjectCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<Common.Models.Task> ProcessedAsync(UpdateTaskCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<Team> ProcessedAsync(UpdateTeamCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<User> ProcessedAsync(UpdateUserCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }

        public async Task<bool> ProcessedAsync(DeleteProjectByIdCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteTaskByIdCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteTeamByIdCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteUserByIdCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteProjectByItemCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteTaskByItemCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteTeamtByItemCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
        public async Task<bool> ProcessedAsync(DeleteUserByItemCommand command)
        {
            return await _commandHandler.HandlerAsync(command);
        }
    }
}
