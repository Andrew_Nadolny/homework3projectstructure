﻿using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.Queries;
using Common.Models;

namespace HomeWork3ProjectStructure.Processors
{
    public class QuerieProcessor : IQuerieProcessor
    {
        public QuerieHandler _querieHandler { get; set; }
        public QuerieProcessor(QuerieHandler querieHandler)
        {
            _querieHandler = querieHandler;
        }

        public async Task<List<Project>> ProcessedAsync(GetProjectsQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }

        public async Task<List<Common.Models.Task>> ProcessedAsync(GetTasksQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }
        public async Task<List<Team>> ProcessedAsync(GetTeamsQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }
        public async Task<List<User>> ProcessedAsync(GetUsersQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }
        public async Task<Project> ProcessedAsync(GetProjectByIdQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }
        public async Task<Team> ProcessedAsync(GetTeamByIdQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }
        public async Task<Common.Models.Task> ProcessedAsync(GetTaskByIdQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }
        public async Task<User> ProcessedAsync(GetUserByIdQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }

        public async Task<Dictionary<int, int>> ProcessedAsync(GetCountTasksInProjectByAuthorIdQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }

        public async Task<List<(int Id, string Name, List<User> Teammates)>> ProcessedAsync(GetListOfTeamsWithTeamatesOlderThen10Querie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }

        public async Task<List<(User User, List<Common.Models.Task> Tasks)>> ProcessedAsync(GetListOfUsersAscendingWhitTasksDescendingQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }

        public async Task<List<(Project Project, Common.Models.Task TaskWithLongestDescriptions, Common.Models.Task TaskWithShortestName, int CountOfUsersInProjects)>> ProcessedAsync(GetProjectWithTaskWithLongestDescriptionsAndETCQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }

        public async Task<List<Common.Models.Task>> ProcessedAsync(GetUserTasksFinishedInCurrentYearQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }

        public async Task<List<Common.Models.Task>> ProcessedAsync(GetUserTasksWithShortNameByUserIdQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }

        public async Task<List<(User User, Project LastProject, List<Common.Models.Task> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, Common.Models.Task LongesUserTask)>> ProcessedAsync(GetUserWithLastProjectAndETCQuerie querie)
        {
            return await _querieHandler.HandlerAsync(querie);
        }
    }
}
