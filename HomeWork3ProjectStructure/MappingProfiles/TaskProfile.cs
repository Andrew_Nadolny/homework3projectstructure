﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using HomeWork3ProjectStructure.DTO;
using Common.Models;

namespace HomeWork3ProjectStructure.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskDTO, Task>().ForMember(x=> x.Performer, s => s.MapFrom(z => new User { Id = z.PerformerId }));
        }
    }
}
