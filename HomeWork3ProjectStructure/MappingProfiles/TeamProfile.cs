﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using HomeWork3ProjectStructure.DTO;
using Common.Models;

namespace HomeWork3ProjectStructure.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>();
        }
    }
}
