﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using HomeWork3ProjectStructure.DTO;
using Common.Models;

namespace HomeWork3ProjectStructure.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>().ForMember(x => x.Team, s => s.MapFrom(z => new Team { Id = z.TeamId })).ForMember(x => x.Author, s => s.MapFrom(z => new User { Id = z.AuthorId }));
        }
    }
}
