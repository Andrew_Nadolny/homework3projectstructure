﻿using Common.Models;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork3ProjectStructure.Commands
{
    public class UpdateTaskCommand : ICommand<Task>
    {
        public Task task { get; set; }
    }
}
