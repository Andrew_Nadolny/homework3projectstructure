﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Models;
using HomeWork3ProjectStructure.Interfaces;

namespace HomeWork3ProjectStructure.Commands
{
    public class DeleteProjectByIdCommand : ICommand<bool>
    {
        public int Id { get; set; }
    }
}
