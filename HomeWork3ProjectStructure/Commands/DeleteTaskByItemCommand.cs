﻿using Common.Models;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork3ProjectStructure.Commands
{
    public class DeleteTaskByItemCommand : ICommand<bool>
    {
        public Task task { get; set; }
    }
}
