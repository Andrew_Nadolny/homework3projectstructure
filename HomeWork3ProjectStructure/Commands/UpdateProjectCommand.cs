﻿using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Common.Models;

namespace HomeWork3ProjectStructure.Commands
{
    public class UpdateProjectCommand : ICommand<Project>
    {
        public Project project { get; set; }
    }
}
