﻿using AutoMapper;
using Common.Models;
using HomeWork3ProjectStructure.DTO;
using HomeWork3ProjectStructure.Interfaces;
using HomeWork3ProjectStructure.Queries;
using HomeWork3ProjectStructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Handlers
{
    public class QuerieHandler : IQuerieHandler<GetProjectsQuerie, List<Project>>,
        IQuerieHandler<GetTasksQuerie, List<Common.Models.Task>>,
        IQuerieHandler<GetTeamsQuerie, List<Team>>,
        IQuerieHandler<GetUsersQuerie, List<User>>,
        IQuerieHandler<GetProjectByIdQuerie, Project>,
        IQuerieHandler<GetTaskByIdQuerie, Common.Models.Task>,
        IQuerieHandler<GetTeamByIdQuerie, Team>
    {
        private readonly HomeWork3ProjectStructure.UnitOfWork.UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public QuerieHandler(HomeWork3ProjectStructure.UnitOfWork.UnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<Project>> HandlerAsync(GetProjectsQuerie querie)
        {
            List<Project> projects = _mapper.Map<List<Project>>(_unitOfWork.Set<ProjectDTO>().Get());
            List<Team> teams = _mapper.Map<List<Team>>(_unitOfWork.Set<TeamDTO>().Get());
            List<Common.Models.Task> tasks = _mapper.Map<List<Common.Models.Task>>(_unitOfWork.Set<TaskDTO>().Get());
            List<User> users = _mapper.Map<List<User>>(_unitOfWork.Set<UserDTO>().Get());

            var taskQuery = from task in tasks
                            join user in users on task.Performer.Id equals user.Id
                            select new Common.Models.Task
                            {
                                TaskState = task.TaskState,
                                Description = task.Description,
                                CreatedAt = task.CreatedAt,
                                FinishedAt = task.FinishedAt,
                                Id = task.Id,
                                Name = task.Name,
                                ProjectId = task.ProjectId,
                                Performer = user
                            };
            var projectsQuery = from project in projects
                                join team in teams on project.Team.Id equals team.Id
                                join task in taskQuery on project.Id equals task.ProjectId into prts
                                join user in users on project.Author.Id equals user.Id
                                select new Project
                                {
                                    Author = user,
                                    CreatedAt = project.CreatedAt,
                                    Deadline = project.Deadline,
                                    Description = project.Description,
                                    Id = project.Id,
                                    Name = project.Name,
                                    Tasks = prts.ToList(),
                                    Team = team
                                };
            return projectsQuery.ToList();

        }
        public async Task<List<Common.Models.Task>> HandlerAsync(GetTasksQuerie querie)
        {
            return _mapper.Map<List<Common.Models.Task>>(_unitOfWork.Set<TaskDTO>().Get());
        }
        public async Task<List<Team>> HandlerAsync(GetTeamsQuerie querie)
        {
            return _mapper.Map<List<Team>>(_unitOfWork.Set<TeamDTO>().Get());
        }
        public async Task<List<User>> HandlerAsync(GetUsersQuerie querie)
        {
            return _mapper.Map<List<User>>(_unitOfWork.Set<UserDTO>().Get());
        }
        public async Task<Project> HandlerAsync(GetProjectByIdQuerie querie)
        {
            return _mapper.Map<Project>(_unitOfWork.Set<ProjectDTO>().Get(querie.Id));
        }
        public async Task<Common.Models.Task> HandlerAsync(GetTaskByIdQuerie querie)
        {
            return _mapper.Map<Common.Models.Task>(_unitOfWork.Set<TaskDTO>().Get(querie.Id));
        }
        public async Task<Team> HandlerAsync(GetTeamByIdQuerie querie)
        {
            return _mapper.Map<Team>(_unitOfWork.Set<TeamDTO>().Get(querie.Id));
        }
        public async Task<User> HandlerAsync(GetUserByIdQuerie querie)
        {
            return _mapper.Map<User>(_unitOfWork.Set<UserDTO>().Get(querie.Id));
        }
        public async Task<Dictionary<int, int>> HandlerAsync(GetCountTasksInProjectByAuthorIdQuerie querie)
        {
            return (await HandlerAsync(new GetProjectsQuerie())).Where(x => x.Author.Id == querie.Id).ToDictionary(x => x.Id, x => x.Tasks.Count());
        }

        public async Task<List<(int Id, string Name, List<User> Teammates)>> HandlerAsync(GetListOfTeamsWithTeamatesOlderThen10Querie querie)
        {
            var projects = (await HandlerAsync(new GetProjectsQuerie()));
            return projects.Select(x => x.Team)
                .Distinct()
                .Select(
                 team => (
                 Id: team.Id,
                 Name: team.Name,
                 Teammates: projects.SelectMany(x => x.Tasks.Select(x => x.Performer)).Union(projects.Select(x => x.Author)).Distinct().Where(x => x.TeamId == team.Id).OrderBy(x => x.RegisteredAt).ToList()
            )).Where(x => !x.Teammates.Any(x => x.BirthDay > DateTime.Now.AddYears(-10))).ToList();
        }

        public async Task<List<(User User, List<Common.Models.Task> Tasks)>> HandlerAsync(GetListOfUsersAscendingWhitTasksDescendingQuerie querie)
        {
            var projects = (await HandlerAsync(new GetProjectsQuerie()));

            return projects.SelectMany(x => x.Tasks).Select(x => x.Performer).Union(projects.Select(x => x.Author)).Distinct().OrderBy(x => x.FirstName.Length)
                .Select(
                user => (
                    User: user,
                    Tasks: projects.SelectMany(x => x.Tasks).Where(x => x.Performer.Id == user.Id).OrderByDescending(x => x.Name.Length).ToList()
            )).ToList();
        }

        public async Task<List<(Project Project, Common.Models.Task TaskWithLongestDescriptions, Common.Models.Task TaskWithShortestName, int CountOfUsersInProjects)>> 
            HandlerAsync(GetProjectWithTaskWithLongestDescriptionsAndETCQuerie querie)
        {
            var projects = (await HandlerAsync(new GetProjectsQuerie()));

            return projects.Select(project => (
            Project: project,
            TaskWithLongestDescriptions: project.Tasks.DefaultIfEmpty().Aggregate((longest, next) =>
                        next.Description.Length > longest.Description.Length ? next : longest),
            TaskWithShortestName: project.Tasks.DefaultIfEmpty().Aggregate((shortests, next) =>
                        next.Name.Length < shortests.Name.Length ? next : shortests),
            CountOfUsersInProjects: (project.Description.Length > 20 || project.Tasks.Count() < 3) ? projects.SelectMany(x => x.Tasks).Select(x => x.Performer).Union(projects.Select(x => x.Author)).Distinct().Where(x => x.TeamId == project.Team.Id).Count() : 0
            )).ToList();
        }

        public async Task<List<Common.Models.Task>> HandlerAsync(GetUserTasksFinishedInCurrentYearQuerie querie)
        {
            return (await HandlerAsync(new GetProjectsQuerie())).SelectMany(x => x.Tasks).Where(x => x.FinishedAt.Year == DateTime.Now.Year && x.Performer.Id == querie.Id).ToList();
        }

        public async Task<List<Common.Models.Task>> HandlerAsync(GetUserTasksWithShortNameByUserIdQuerie querie)
        {
            return (await HandlerAsync(new GetProjectsQuerie())).SelectMany(x => x.Tasks).Where(x => x.Performer.Id == querie.Id && x.Name.Length < 45).ToList();
        }

        public async Task<List<(User User, Project LastProject, List<Common.Models.Task> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, Common.Models.Task LongesUserTask)>> HandlerAsync(GetUserWithLastProjectAndETCQuerie querie)
        {
            var projects = (await HandlerAsync(new GetProjectsQuerie()));

            return  projects.SelectMany(project => project.Tasks).Select(task => task.Performer).Union(projects.Select(x => x.Author)).Distinct().Select(user => (
                User: user,
                LastProject: projects.Where(project => project.Author.Id == user.Id).OrderBy(project => project.CreatedAt)?.FirstOrDefault(),
                LastProjectTask: projects.Where(project => project.Author.Id == user.Id).OrderBy(project => project.CreatedAt).FirstOrDefault()?.Tasks,
                CountOfUnfinishedAndCanceledTasks: projects.SelectMany(project => project.Tasks).Where(x => x.Performer.Id == user.Id && (x.FinishedAt != DateTime.MinValue || x.TaskState == TaskState.Canceled)).Count(),
                LongesUserTask: projects.SelectMany(project => project.Tasks).Where(task => task.Performer.Id == user.Id).DefaultIfEmpty().Aggregate((longest, next) =>
                       (next.FinishedAt - next.CreatedAt) > (longest.FinishedAt - longest.CreatedAt) ? next : longest)
            )).ToList();
        }
    }
}
