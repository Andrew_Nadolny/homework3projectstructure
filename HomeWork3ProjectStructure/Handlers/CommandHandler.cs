﻿using AutoMapper;
using Common.Models;
using HomeWork3ProjectStructure.Commands;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.UnitOfWork;
using HomeWork3ProjectStructure.Interfaces;
using HomeWork3ProjectStructure.DTO;

namespace HomeWork3ProjectStructure.Handlers
{
    public class CommandHandler : ICommandHandler<CreateProjectCommand, Project>,
          ICommandHandler<CreateTaskCommand, Common.Models.Task>,
          ICommandHandler<CreateTeamCommand, Team>,
          ICommandHandler<CreateUserCommand, User>,
          ICommandHandler<UpdateProjectCommand, Project>,
          ICommandHandler<UpdateTaskCommand, Common.Models.Task>,
          ICommandHandler<UpdateTeamCommand, Team>,
          ICommandHandler<UpdateUserCommand, User>,
          ICommandHandler<DeleteProjectByIdCommand, bool>,
          ICommandHandler<DeleteTaskByIdCommand, bool>,
          ICommandHandler<DeleteTeamByIdCommand, bool>,
          ICommandHandler<DeleteUserByIdCommand, bool>,
          ICommandHandler<DeleteProjectByItemCommand, bool>,
          ICommandHandler<DeleteTaskByItemCommand, bool>,
          ICommandHandler<DeleteTeamtByItemCommand, bool>,
          ICommandHandler<DeleteUserByItemCommand, bool>

    {
        private readonly HomeWork3ProjectStructure.UnitOfWork.UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommandHandler(HomeWork3ProjectStructure.UnitOfWork.UnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Project> HandlerAsync(CreateProjectCommand command)
        {
            return _mapper.Map<Project>(_unitOfWork.Set<ProjectDTO>().Create(_mapper.Map<ProjectDTO>(command.project)));
        }

        public async Task<Common.Models.Task> HandlerAsync(CreateTaskCommand command)
        {
            return _mapper.Map<Common.Models.Task>(_unitOfWork.Set<TaskDTO>().Create(_mapper.Map<TaskDTO>(command.task)));
        }

        public async Task<Team> HandlerAsync(CreateTeamCommand command)
        {
            return _mapper.Map<Team>(_unitOfWork.Set<TeamDTO>().Create(_mapper.Map<TeamDTO>(command.team)));
        }

        public async Task<User> HandlerAsync(CreateUserCommand command)
        {
            return _mapper.Map<User>(_unitOfWork.Set<UserDTO>().Create(_mapper.Map<UserDTO>(command.user)));
        }

        public async Task<Project> HandlerAsync(UpdateProjectCommand command)
        {
            return _mapper.Map<Project>(_unitOfWork.Set<ProjectDTO>().Update(_mapper.Map<ProjectDTO>(command.project)));
        }

        public async Task<Common.Models.Task> HandlerAsync(UpdateTaskCommand command)
        {
            return _mapper.Map<Common.Models.Task>(_unitOfWork.Set<TaskDTO>().Update(_mapper.Map<TaskDTO>(command.task)));
        }

        public async Task<Team> HandlerAsync(UpdateTeamCommand command)
        {
            return _mapper.Map<Team>(_unitOfWork.Set<TeamDTO>().Update(_mapper.Map<TeamDTO>(command.team)));
        }

        public async Task<User> HandlerAsync(UpdateUserCommand command)
        {
            return _mapper.Map<User>(_unitOfWork.Set<UserDTO>().Update(_mapper.Map<UserDTO>(command.user)));
        }

        public async Task<bool> HandlerAsync(DeleteProjectByIdCommand command)
        {
           return _unitOfWork.Set<ProjectDTO>().Delete(command.Id);
        }

        public async Task<bool> HandlerAsync(DeleteTaskByIdCommand command)
        {
            return  _unitOfWork.Set<TaskDTO>().Delete(command.Id);
        }

        public async Task<bool> HandlerAsync(DeleteTeamByIdCommand command)
        {
            return _unitOfWork.Set<TeamDTO>().Delete(command.Id);
        }

        public async Task<bool> HandlerAsync(DeleteUserByIdCommand command)
        {
            return _unitOfWork.Set<UserDTO>().Delete(command.Id);
        }

        public async Task<bool> HandlerAsync(DeleteProjectByItemCommand command)
        {
            return _unitOfWork.Set<ProjectDTO>().Delete(_mapper.Map<ProjectDTO>(command.project));
        }

        public async Task<bool> HandlerAsync(DeleteTaskByItemCommand command)
        {
            return _unitOfWork.Set<TaskDTO>().Delete(_mapper.Map<TaskDTO>(command.task));
        }

        public async Task<bool> HandlerAsync(DeleteTeamtByItemCommand command)
        {
            return _unitOfWork.Set<UserDTO>().Delete(_mapper.Map<UserDTO>(command.team));
        }

        public async Task<bool> HandlerAsync(DeleteUserByItemCommand command)
        {
            return _unitOfWork.Set<UserDTO>().Delete(_mapper.Map<UserDTO>(command.user));
        }


    }
}
