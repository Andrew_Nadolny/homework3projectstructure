﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.DTO;

namespace HomeWork3ProjectStructure.Repositores
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        public List<TEntity> entities { get; }

        public Repository()
        {
            entities = DataBase.Get<TEntity>();
        }
        public List<TEntity> Get()
        {
            return entities;
        }

        public TEntity Get(int id)
        {
            return entities.SingleOrDefault(x => x.Id == id);
        }

        public TEntity Create(TEntity entity)
        {
            entities.Add(entity);
            return entity;
        }

        public bool Delete(TEntity entity)
        {
            entities.Remove(entity);
            return !entities.Any(x => x == entity);
        }

        public bool Delete(int id)
        {
            entities.Remove(entities.SingleOrDefault(x=> x.Id == id));
            return !entities.Any(x => x.Id == id);
        }

        public TEntity Update(TEntity entity)
        {
            entities.Where(x => x.Id == entity.Id)?.Select(x=> x == entity);
            return entity;
        }
    }
}
