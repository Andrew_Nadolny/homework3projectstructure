﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.DTO;

namespace HomeWork3ProjectStructure.Repositores
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        List<TEntity> Get();

        TEntity Get(int Id);

        TEntity Create(TEntity entity);

        TEntity Update(TEntity entity);

        bool Delete(int id);

        bool Delete(TEntity entity);
    }
}
