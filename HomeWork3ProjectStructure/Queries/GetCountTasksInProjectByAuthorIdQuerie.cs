﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Models;
using HomeWork3ProjectStructure.Interfaces;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetCountTasksInProjectByAuthorIdQuerie : IQuerie<Dictionary<int, int>>
    {
        public int Id { get; set; }
    }
}
