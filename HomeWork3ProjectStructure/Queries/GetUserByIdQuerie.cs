﻿using Common.Models;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetUserByIdQuerie : IQuerie<User>
    {
        public int Id { get; set; }
    }
}
