﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Models;
using HomeWork3ProjectStructure.Interfaces;


namespace HomeWork3ProjectStructure.Queries
{
    public class GetTaskByIdQuerie : IQuerie<Task>
    {
        public int Id { get; set; }

    }
}
