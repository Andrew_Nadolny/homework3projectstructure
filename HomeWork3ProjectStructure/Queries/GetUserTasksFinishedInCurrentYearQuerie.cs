﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomeWork3ProjectStructure.Interfaces;
using Common.Models;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetUserTasksFinishedInCurrentYearQuerie : IQuerie<List<Task>>
    {
        public int Id { get; set; }
    }
}
