﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Models;
using HomeWork3ProjectStructure.Interfaces;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetProjectByIdQuerie : IQuerie<Project>
    {
        public int Id { get; set; }
    }
}
