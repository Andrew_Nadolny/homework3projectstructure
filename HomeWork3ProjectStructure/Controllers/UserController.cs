﻿using AutoMapper;
using Common.Models;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using HomeWork3ProjectStructure.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty users, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3UserStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly QuerieProcessor _querieProcessor;
        private readonly CommandProcessor _commandProcessor;

        public UserController(UnitOfWork unitOfWork, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(unitOfWork, mapper));
            _commandProcessor = new CommandProcessor(new CommandHandler(unitOfWork, mapper));

        }
        // GET: api/<UserController>
        [HttpGet]
        public async Task<string> Get()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetUsersQuerie()));
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public async Task<string> GetAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetTaskByIdQuerie() { Id = id }));
        }

        // POST api/<UserController>
        [HttpPost]
        public async System.Threading.Tasks.Task<string> PostAsync([FromBody] User user)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new CreateUserCommand() { user = user }));
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task<string> Put([FromBody] User user)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new UpdateUserCommand() { user = user }));

        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<bool> DeleteAsync(int id)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteUserByIdCommand() { Id = id });
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<bool> Delete([FromBody] User user)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteUserByItemCommand() { user = user });

        }
    }
}
