﻿using AutoMapper;
using Common.Models;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using HomeWork3ProjectStructure.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty teams, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3TeamStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        // GET: api/<TeamController>
        private readonly QuerieProcessor _querieProcessor;
        private readonly CommandProcessor _commandProcessor;

        public TeamController(UnitOfWork unitOfWork, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(unitOfWork, mapper));
            _commandProcessor = new CommandProcessor(new CommandHandler(unitOfWork, mapper));

        }
        // GET: api/<TeamController>
        [HttpGet]
        public async Task<string> Get()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetTeamsQuerie()));
        }

        // GET api/<TeamController>/5
        [HttpGet("{id}")]
        public async Task<string> GetAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetTaskByIdQuerie() { Id = id }));
        }

        // POST api/<TeamController>
        [HttpPost]
        public async System.Threading.Tasks.Task<string> PostAsync([FromBody] Team team)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new CreateTeamCommand() { team = team }));
        }

        // PUT api/<TeamController>/5
        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task<string> Put([FromBody] Team team)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new UpdateTeamCommand() { team = team }));

        }

        // DELETE api/<TeamController>/5
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<bool> DeleteAsync(int id)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteTeamByIdCommand() { Id = id });
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<bool> Delete([FromBody] Team team)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteTeamtByItemCommand() { team = team });

        }
    }
}
