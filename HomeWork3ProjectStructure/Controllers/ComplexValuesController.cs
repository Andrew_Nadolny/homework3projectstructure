﻿using AutoMapper;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComplexValuesController : ControllerBase
    {

        private readonly QuerieProcessor _querieProcessor;

        public ComplexValuesController(HomeWork3ProjectStructure.UnitOfWork.UnitOfWork unitOfWork, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(unitOfWork, mapper));

        }

        [Route("GetCountTasksInProjectByAuthorId")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async Task<string> GetCountTasksInProjectByAuthorId(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetCountTasksInProjectByAuthorIdQuerie() { Id = id }));
        }

        [Route("GetListOfTeamsWithTeamatesOlderThen10")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async Task<string> GetListOfTeamsWithTeamatesOlderThen10()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetListOfTeamsWithTeamatesOlderThen10Querie()));
        }

        [Route("GetListOfUsersAscendingWhitTasksDescending")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async Task<string> GetListOfUsersAscendingWhitTasksDescending()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetListOfUsersAscendingWhitTasksDescendingQuerie()));
        }

        [Route("GetProjectWithTaskWithLongestDescriptionsAndETC")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async Task<string> GetProjectWithTaskWithLongestDescriptionsAndETC()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetProjectWithTaskWithLongestDescriptionsAndETCQuerie()));
        }

        [Route("GetUserWithLastProjectAndETC")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async Task<string> GetUserWithLastProjectAndETC()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetUserWithLastProjectAndETCQuerie()));
        }


        [Route("GetUserTasksWithShortNameByUserId")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async Task<string> GetUserTasksWithShortNameByUserId(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetUserTasksWithShortNameByUserIdQuerie() { Id = id }));
        }

        [Route("GetUserTasksFinishedInCurrentYear")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public async Task<string> GetUserTasksFinishedInCurrentYear(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetUserTasksFinishedInCurrentYearQuerie() { Id = id }));
        }
    }
}
