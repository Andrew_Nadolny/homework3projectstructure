﻿using AutoMapper;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.UnitOfWork;
using HomeWork3ProjectStructure.Handlers;

// For more information on enabling Web API for empty tasks, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3TaskStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly QuerieProcessor _querieProcessor;
        private readonly CommandProcessor _commandProcessor;

        public TaskController(UnitOfWork unitOfWork, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(unitOfWork, mapper));
            _commandProcessor = new CommandProcessor(new CommandHandler(unitOfWork, mapper));

        }
        // GET: api/<TaskController>
        [HttpGet]
        public async Task<string> Get()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetTasksQuerie()));
        }

        // GET api/<TaskController>/5
        [HttpGet("{id}")]
        public async Task<string> GetAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetTaskByIdQuerie() { Id = id }));
        }

        // POST api/<TaskController>
        [HttpPost]
        public async System.Threading.Tasks.Task<string> PostAsync([FromBody] Common.Models.Task task)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new CreateTaskCommand() { task = task }));
        }

        // PUT api/<TaskController>/5
        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task<string> Put([FromBody] Common.Models.Task task)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new UpdateTaskCommand() { task = task }));

        }

        // DELETE api/<TaskController>/5
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<bool> DeleteAsync(int id)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteTaskByIdCommand() { Id = id });
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<bool> Delete([FromBody] Common.Models.Task task)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteTaskByItemCommand() { task = task });

        }

    }
}
