﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class User : IModel
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public override string ToString()
        {
            return string.Format("\nUser: [Id:{0}, TeamId:{1}, FirstName:{2}, LastName:{3}, Email:{4}, RegisteredAt:{5}, BirthDay:{6} ]\n", Id, TeamId, FirstName, LastName, Email, RegisteredAt, BirthDay);
        }
    }
}
