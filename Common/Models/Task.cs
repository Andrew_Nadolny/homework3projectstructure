﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class Task : IModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState TaskState { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }

        public override string ToString()
        {
            return string.Format("\nTask: [Id:{0}, ProjectId:{1}, Performer:{2}, Name:{3}, Description:{4}, TaskState:{5}, CreatedAt:{6}, FinishedAt:{7} ]\n",
                Id,
                ProjectId,
                Performer.FirstName +" "+ Performer.LastName,
                Name,
                Description,
                TaskState,
                CreatedAt,
                FinishedAt);
        }
    }
}
