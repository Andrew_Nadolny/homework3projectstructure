﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Models
{
    public class Project : IModel
    {
        public int Id { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return string.Format("\nProject: [Id:{0}, Author:{1}, Team:{2}, Tasks:{3}, Name:{4}, Description:{5}, Deadline:{6}, CreateAt:{7}]\n",
                Id,
                Author,
                Team.Name,
                string.Join(", ", Tasks.Select(x => x.Name)),
                Name,
                Description,
                Deadline,
                CreatedAt);
        }
    }
}
